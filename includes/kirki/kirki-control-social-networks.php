<?php

/**
 * Kirki Customizer - List of Social Networks
 *
 */

new \Kirki\Section(
	'tucan_section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'tucan'),
		'description' => esc_html__('Social networks to use around the site.', 'tucan'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'tucan_setting_social_facebook',
		'label'    => esc_html__('Facebook', 'tucan'),
		'section'  => 'tucan_section_social_networks',
		'default'  => 'https://www.facebook.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'tucan_setting_social_instagram',
		'label'    => esc_html__('Instagram', 'tucan'),
		'section'  => 'tucan_section_social_networks',
		'default'  => 'https://www.instagram.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'tucan_setting_social_whatsapp',
		'label'    => esc_html__('Whatsapp', 'tucan'),
		'section'  => 'tucan_section_social_networks',
		'default'  => 'https://api.whatsapp.com/send?phone=5511988887777&text=Ol%C3%A1',
		'priority' => 10,
	)
);
