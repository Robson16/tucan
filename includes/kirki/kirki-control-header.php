<?php

/**
 * Kirki Customizer - Footer
 *
 */

new \Kirki\Panel(
	'tucan_panel_header',
	array(
		'priority'    => 160,
		'title'       => esc_html__('Header', 'tucan'),
		'description' => esc_html__('Extra options to customize the header.', 'tucan'),
	)
);

new \Kirki\Section(
	'tucan_section_header_logos',
	array(
		'title'       => esc_html__('Logos', 'tucan'),
		'description' => esc_html__('Custom logo for the header navbar.', 'tucan'),
		'panel'       => 'tucan_panel_header',
		'priority'    => 160,
	)
);

new \Kirki\Field\Image(
	array(
		'settings'    => 'tucan_section_header_logo',
		'label'       => esc_html__('Normal Logo', 'tucan'),
		'section'     => 'tucan_section_header_logos',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

new \Kirki\Field\Image(
	array(
		'settings'    => 'tucan_section_header_logo_sticky',
		'label'       => esc_html__('Sticky Logo', 'tucan'),
		'description' => esc_html__('Logo to show when the navbar is on mode stick.', 'tucan'),
		'section'     => 'tucan_section_header_logos',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

new \Kirki\Field\Image(
	array(
		'settings'    => 'tucan_section_header_logo_corp',
		'label'       => esc_html__('Corp Logo', 'tucan'),
		'description' => esc_html__('Logo to show on the template page Corp.', 'tucan'),
		'section'     => 'tucan_section_header_logos',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);
