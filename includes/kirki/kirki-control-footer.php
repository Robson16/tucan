<?php

/**
 * Kirki Customizer - Footer
 *
 */

new \Kirki\Panel(
	'tucan_panel_footer',
	array(
		'title'       => esc_html__('Footer', 'tucan'),
		'description' => esc_html__('Extra options to customize the footer.', 'tucan'),
		'priority'    => 160,
	)
);

// Logo
new \Kirki\Section(
	'tucan_section_footer_logo',
	array(
		'title'       => esc_html__('Logo', 'tucan'),
		'panel'       => 'tucan_panel_footer',
		'priority'    => 160,
	)
);

new \Kirki\Field\Image(
	array(
		'settings'    => 'tucan_setting_footer_logo',
		'label'       => esc_html__('Logo', 'tucan'),
		'description' => esc_html__('Logo to show on the footer.', 'tucan'),
		'section'     => 'tucan_section_footer_logo',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		),
	)
);

// Address
new \Kirki\Section(
	'tucan_section_footer_address',
	array(
		'title'       => esc_html__('Address', 'tucan'),
		'panel'       => 'tucan_panel_footer',
		'priority'    => 160,
	)
);

new \Kirki\Field\Textarea(
	array(
		'settings'    => 'tucan_setting_footer_address_text',
		'label'       => esc_html__('Address', 'tucan'),
		'section'     => 'tucan_section_footer_address',
		'default'     => '',
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'tucan_setting_footer_address_url',
		'label'    => esc_html__('Address - URL', 'tucan'),
		'section'  => 'tucan_section_footer_address',
		'default'  => 'https://maps.app.goo.gl/UwzTmc1qkHXQbkT18',
		'priority' => 10,
	)
);

// CTA
new \Kirki\Section(
	'tucan_section_footer_cta',
	array(
		'title'       => esc_html__('CTA', 'tucan'),
		'description' => esc_html__('Call To Action Button', 'tucan'),
		'panel'       => 'tucan_panel_footer',
		'priority'    => 160,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'tucan_setting_footer_cta_text',
		'label'    => esc_html__('CTA - Text', 'tucan'),
		'section'  => 'tucan_section_footer_cta',
		'default'  => esc_html__('Create itinerary', 'tucan'),
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'tucan_setting_footer_cta_url',
		'label'    => esc_html__('CTA - URL', 'tucan'),
		'section'  => 'tucan_section_footer_cta',
		'default'  => '',
		'priority' => 10,
	)
);
