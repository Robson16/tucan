<?php

/**
 * Kirk Customizer Plugin configurations
 *
 * @link https://kirki.org/
 *
 */

add_action('after_setup_theme', function () {
	if (class_exists('kirki')) {
		require_once get_template_directory() . '/includes/kirki/kirki-control-header.php';
		require_once get_template_directory() . '/includes/kirki/kirki-control-social-networks.php';
		require_once get_template_directory() . '/includes/kirki/kirki-control-footer.php';
	}
}, 20);
