<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<header id="header" class="header corp">
		<div class="header-placeholder" style="height: 0px;"></div>

		<nav id="navbar" class="navbar">
			<div class="container">
				<span class="navbar-brand">
					<?php
					$home_url = (function_exists('pll_count_posts'))
						? (string) pll_home_url(pll_current_language('slug'))
						: get_home_url();
					?>

					<a href="<?php echo $home_url; ?>" class="navbar-brand-link">
						<?php $header_logo = get_theme_mod('tucan_section_header_logo_corp', ''); ?>

						<?php if ($header_logo) : ?>
							<img class="custom-logo" src="<?php echo esc_url($header_logo['url']); ?>" width="<?php $header_logo['width'] ?>" height="<?php $header_logo['height'] ?>" alt="<?php echo get_bloginfo('title'); ?>">
						<?php else : ?>
							<h1 class="site-title" style="margin: 0;">
								<?php echo get_bloginfo('title'); ?>
							</h1>
						<?php endif; ?>
					</a>
				</span>

				<?php if (has_nav_menu('main_menu')) : ?>
					<button type="button" class="navbar-toggler" data-target="#navbar-nav">
						<span class="navbar-toggler-icon">
							<div class="bar1"></div>
							<div class="bar2"></div>
							<div class="bar3"></div>
						</span>
					</button>

					<?php
					wp_nav_menu(array(
						'theme_location'      => 'main_menu',
						'depth'               => 2,
						'container'           => 'div',
						'container_class'     => 'collapse navbar-collapse',
						'container_id'        => 'navbar-nav',
						'menu_class'          => 'navbar-nav',
						'fallback_cb'         => 'WP_Bootstrap_Navwalker::fallback',
						'walker'              => new WP_Bootstrap_Navwalker()
					));
					?>
				<?php endif; ?>
			</div>
			<!-- /.container -->
		</nav>
		<!-- /.navbar -->
	</header>

	<main>