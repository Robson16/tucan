<?php
/*
Template Name: Corp
Template Post Type: page
*/

get_header('corp');

while (have_posts()) {
	the_post();
	get_template_part('partials/content/content', 'page');
}

get_footer();
