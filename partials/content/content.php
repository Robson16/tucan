<?php

/**
 * Generic template part to display publication
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		the_post_thumbnail("full", array("title" => get_the_title()));

		the_title("<h1 class='entry-title'>", "</h1>");
		?>

		<p class="entry-meta">
			<?php echo date_i18n(get_option('date_format'), get_post_timestamp()); ?>

			&#47;

			<?php echo get_the_author_meta('display_name'); ?>

			&#47;

			<?php
			$categories = get_categories(array(
				'orderby' => 'name',
				'parent'  => 0
			));

			foreach ($categories as $category) {
				printf(
					'<a href="%1$s">%2$s</a>, ',
					esc_url(get_category_link($category->term_id)),
					esc_html($category->name)
				);
			}
			?>
		</p>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->

	<footer class="entry-footer">
		<hr>
		<p>
			<?php _e('Posted in', 'tucan'); ?>
			<?php the_date(); ?>
			<br>
			<?php _e('Categories:', 'tucan'); ?>
			<?php the_category(', '); ?>
			<br>
			<?php if (has_tag()) : ?>
				<?php _e('Tags:', 'tucan'); ?>
				<?php the_tags(' ', ', ', ' '); ?>
			<?php endif; ?>
		</p>
		<hr>
	</footer>
</article><!-- #post-<?php the_ID(); ?> -->