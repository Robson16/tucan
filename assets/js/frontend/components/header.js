export default class Header {
	constructor() {
		this.body = document.querySelector('body');

		this.wpAdminBar = document.querySelector('#wpadminbar');
		this.wpAdminBarHeight = 0;

		this.header = document.querySelector('#header');
		this.headerHeight = 0;

		this.headerPlaceholder =
			this.header.querySelector('.header-placeholder') ?? undefined;

		this.navbar = document.querySelector('#navbar');
		this.navbarHeight = 0;

		this.navbarCollapse =
			this.navbar.querySelector('.navbar-collapse') ?? undefined;

		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;

		this.offset = this.wpAdminBarHeight + this.navbar.offsetHeight;

		this.events();
	}

	events() {
		if (this.navbar) {
			['resize', 'load'].forEach((event) => {
				window.addEventListener(event, () => {
					this.handleSizesReCalc();
					this.handleLoggedIn();
					this.handleNavbarCollapseSize();
				});
			});

			window.addEventListener('scroll', () => {
				this.handleStickyEffect();
			});

			this.handleNavbarToggle();
		}
	}

	handleSizesReCalc() {
		this.wpAdminBarHeight = this.wpAdminBar
			? this.wpAdminBar.offsetHeight
			: 0;

		this.navbarHeight = this.navbar ? this.navbar.offsetHeight - 1 : 0;

		this.headerPlaceholder.style.height = `${this.navbarHeight}px`;

		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;
	}

	handleNavbarCollapseSize() {
		if (this.navbarCollapse) {
			if (this.viewportX < 992) {
				this.navbarCollapse.style.height = `calc(100vh - ${this.navbarHeight}px)`;
				this.navbarCollapse.style.top = `${this.navbarHeight}px`;
			} else {
				this.navbarCollapse.style.height = null;
				this.navbarCollapse.style.top = null;
			}
		}
	}

	handleLoggedIn() {
		if (this.wpAdminBar) {
			this.navbar.style.top = `${this.wpAdminBarHeight}px`;
		} else {
			this.navbar.style.top = 0;
		}
	}

	handleStickyEffect() {
		const isHeaderCorp = this.header.classList.contains('corp');

		if (!isHeaderCorp) {
			this.navbar.classList.toggle(
				'is-sticky',
				window.scrollY > this.offset
			);
		}
	}

	handleNavbarToggle() {
		const navbarToggler = this.navbar.querySelector('.navbar-toggler');

		if (navbarToggler) {
			const icon = navbarToggler.querySelector('.navbar-toggler-icon');
			let target = navbarToggler.dataset.target;

			target = document.querySelector(target);

			if (target) {
				navbarToggler.addEventListener('click', () => {
					this.body.classList.toggle('no-scroll-vertical');

					navbarToggler.classList.toggle('show');
					target.classList.toggle('show');
					icon.classList.toggle('close');
				});
			}
		}
	}
}
