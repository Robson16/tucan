<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

</main>

<footer id="footer" class="footer">
	<div class="container">
		<div class="footer__column footer__logo">
			<?php
			$footer_logo = get_theme_mod('tucan_setting_footer_logo', '');

			if ($footer_logo) :
			?>
				<img class="custom-logo" src="<?php echo esc_url($footer_logo['url']); ?>" width="<?php $footer_logo['width'] ?>" height="<?php $footer_logo['height'] ?>" alt="<?php echo get_bloginfo('title'); ?>">
			<?php endif; ?>
		</div>
		<!-- /.footer__logo -->

		<div class="footer__column footer__about">
			<?php
			$footer_address_text = get_theme_mod('tucan_setting_footer_address_text', '');
			$footer_address_url = get_theme_mod('tucan_setting_footer_address_url', 'https://maps.app.goo.gl/UwzTmc1qkHXQbkT18');

			if ($footer_address_text) :
			?>
				<div class="address">
					<strong><?php esc_html_e('Address', 'tucan'); ?></strong>
					<a href="<?php echo esc_url($footer_address_url); ?>" target="_blank">
						<span><?php echo esc_html($footer_address_text); ?></span>
					</a>
				</div>
			<?php endif; ?>

			<?php get_template_part('partials/footer/footer', 'social-networks'); ?>
		</div>
		<!-- /.footer__about -->

		<div class="footer__column footer__nav">
			<?php
			if (has_nav_menu('footer_menu')) {
				wp_nav_menu(array(
					'theme_location'  => 'footer_menu',
					'depth'           => 1,
					'container_class' => 'footer-menu-wrap',
					'menu_class'      => 'footer-menu',
				));
			}
			?>

			<?php
			$footer_cta_text = get_theme_mod('tucan_setting_footer_cta_text', esc_html__('Create itinerary', 'tucan'));
			$footer_cta_url = get_theme_mod('tucan_setting_footer_cta_url', '');

			if ($footer_cta_url) :
			?>
				<a class="footer-cta" href="<?php echo esc_url($footer_cta_url); ?>" target="">
					<?php
					echo esc_html($footer_cta_text);
					echo file_get_contents(get_template_directory_uri() . '/assets/svg/arrow-right.svg');
					?>
				</a>
			<?php endif; ?>
		</div>
		<!-- /.footer__nav -->

		<div class="footer__column footer__copyright">
			<span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo get_bloginfo('title'); ?>&nbsp;&#x2010;&nbsp;<?php esc_html_e('All rights reserved.', 'tucan') ?></span>
		</div>
		<!-- /.footer__copyright -->

		<div class="footer__column footer__developer">
			<span><?php echo sprintf("%s %s.", esc_html__('Developed by:', 'tucan'), '<a href="https://www.agenciabeb.com.br/" target="_blank">Agência B&amp;B</a>'); ?></span>
		</div>
		<!-- /.footer__developer -->
	</div>
	<!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>